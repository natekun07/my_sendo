import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:my_sendo/screen/account_screen.dart';
import 'package:my_sendo/screen/category_screen.dart';
import 'package:my_sendo/screen/pay_screen.dart';
import 'package:my_sendo/screen/splash_screen.dart';
import 'package:my_sendo/screen/suggest_screen.dart';

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        home: SplashScreen(),
    );
  }
}

class MyMainStatefull extends StatefulWidget {
  const MyMainStatefull({Key? key}) : super(key: key);

  @override
  State<MyMainStatefull> createState() => _MyMainStatefullState();
}


class _MyMainStatefullState extends State<MyMainStatefull> {
  int _selectedIndex = 0;
  final screens = [
    SuggestScreen(),
    PayScreen(),
    CategoryScreen(),
    AccountScreen(),

  ];


  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: screens[_selectedIndex],
      bottomNavigationBar: BottomNavigationBar(
        backgroundColor: Colors.white,
        type: BottomNavigationBarType.fixed,
        showUnselectedLabels: false,

        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.favorite),
            label: 'Gợi ý',
          ),

          BottomNavigationBarItem(
            icon: Icon(Icons.payment),
            label: 'ví ưu đãi',
          ),

          BottomNavigationBarItem(
            icon: Icon(Icons.category),
            label: 'Danh mục',
          ),

          BottomNavigationBarItem(
            icon: Icon(Icons.account_box),
            label: 'Cá nhân',
          ),


        ],
        currentIndex: _selectedIndex,
        selectedItemColor: Colors.red,
        onTap: _onItemTapped,
      ),
    );
  }
}
