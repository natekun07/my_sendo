import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:my_sendo/screen/registrationscreen.dart';

import 'Loginscreen.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.grey,
      ),
      backgroundColor: Colors.white,
      body: Column(
          children: [
            SizedBox(
              height: 300,
              child: Image.network(
                "https://image.thanhnien.vn/w2048/Uploaded/2022/urwqwc.zl/2021_05_11/sendo/sendo_1_vkno.jpg",
                fit: BoxFit.contain,
              ),
            ),
            Expanded(
              flex: 3,
              child: Column(
                children: [
                  SizedBox(
                    child: Text('Kết nối với Sendo để xem gợi ý mua sắm dành riêng cho bạn.',textAlign: TextAlign.center,),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                   children: [
                     Material(
                       color: Colors.grey,
                       child: MaterialButton(
                           onPressed: () {
                             Navigator.push(
                               context,
                               MaterialPageRoute(builder: (context) => const LoginScreen()),
                             );
                           },
                           child: Text(
                             'Đăng nhập',
                             style: TextStyle
                               (
                                 fontSize: 20,
                                 color: Colors.white,
                                 fontWeight: FontWeight.bold
                             ),
                           )
                       ),
                     ),
                     SizedBox(
                       width: 10,
                     ),
                     Material(
                       elevation: 5,
                       color: Colors.red,
                       child: MaterialButton(
                           onPressed: () {
                             Navigator.push(
                               context,
                               MaterialPageRoute(builder: (context) => const RegistrationScreen()),
                             );
                           },
                           child: Text(
                             'Đăng ký',
                             style: TextStyle
                               (
                                 fontSize: 20,
                                 color: Colors.white,
                                 fontWeight: FontWeight.bold
                             ),
                           )
                       ),
                     ),
                   ],
                 ),

                ],
              ),),
          ],
        ),
    );
  }
}
