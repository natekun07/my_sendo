import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../models/user_model.dart';
import 'Loginscreen.dart';


class AccountScreen extends StatefulWidget {
  const AccountScreen({Key? key}) : super(key: key);

  @override
  State<AccountScreen> createState() => _AccountScreenState();
}

class _AccountScreenState extends State<AccountScreen> {
  User? user = FirebaseAuth.instance.currentUser;
  UserModel loggedInUser =  UserModel();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    FirebaseFirestore.instance.collection("users").doc(user!.uid).get().then((value) {
      this.loggedInUser = UserModel.fromMap(value.data());
      setState(() {

      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: Center(
          child: Text(
            "Cá nhân",
          ),
        ),
        backgroundColor: Colors.red,
      ),
      body: Column(
        children: [
          Row(
            children: [
              MaterialButton(
                onPressed: () {},
                shape: CircleBorder(),
                child: const CircleAvatar(
                  backgroundImage: null,
                ),
              ),
              SizedBox(width: 30,),
              Column(
                children: [
                  Text('${loggedInUser.firstname} ${loggedInUser.secondName}'),
                  Text('${loggedInUser.email}'),
                ],
              ),
              SizedBox(width: 100,),
              TextButton(
                onPressed: (){
                  logout(context);
                  },
                style: TextButton.styleFrom(
                  backgroundColor: Colors.redAccent,
                  primary: Colors.white,
                ),
                child: Text('Log out'),
              ),
            ],
          ),
        ],
      ),
    );
  }
  Future<void> logout(BuildContext context) async {
    await FirebaseAuth.instance.signOut();
    Navigator.of(context).pushReplacement(
        MaterialPageRoute(builder: (context) => LoginScreen())
    );
  }
}
