import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:my_sendo/screen/registrationscreen.dart';

import '../ui/app.dart';


class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {

  //from key
  final _formKey = GlobalKey<FormState>();

  //editing controller

  final TextEditingController emailController = new TextEditingController();
  final TextEditingController passwordController = new TextEditingController();


  final _auth = FirebaseAuth.instance;
  
  @override
  Widget build(BuildContext context) {

    //email field
    final emailField = TextFormField(
      autofocus: false,
      controller: emailController,
      keyboardType: TextInputType.emailAddress,

      validator: (value) {
        if(value!.isEmpty){
          return ("Email bạn đang trống!");
        }
        if(!RegExp("^[a-zA-Z0-9+_.-]+@[a-zA-Z0-9.-]+.[a-z]"
        ).hasMatch(value)){
          return ("Bạn cần viết đúng định dạng email");
        }
          return null;
      },
      onSaved: (value) {
        emailController.text = value!;
        },
      textInputAction: TextInputAction.next,
      decoration: InputDecoration(
        contentPadding: EdgeInsets.fromLTRB(20, 15, 20, 15),
        hintText: "Email",
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(5),
        )
      ),
    );

    //passsword field
    final passwordField = TextFormField(
      autofocus: false,
      obscureText: true,
      controller: passwordController,

      validator: (value) {
        RegExp regex = new RegExp(r'^.{6,}$');
        if(value!.isEmpty){
          return("Bạn chưa nhập mật khẩu");
        }
        if(!regex.hasMatch(value))
          {
            return("Mật khẩu cần ít nhất 6 ký tự!");
          }
      },

      onSaved: (value) {
        passwordController.text = value!;
      },
      textInputAction: TextInputAction.next,
      decoration: InputDecoration(
          contentPadding: EdgeInsets.fromLTRB(20, 15, 20, 15),
          hintText: "Mật khẩu",
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(5),
          )
      ),
    );

    //Button
    final loginButton = Material(
      elevation: 5,
      borderRadius: BorderRadius.circular(10),
      color: Colors.red,
      child: MaterialButton(
        padding: EdgeInsets.fromLTRB(20, 15, 20, 15),
        minWidth: MediaQuery.of(context).size.width,
        onPressed: (){
          signIn(emailController.text, passwordController.text);
        },
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              'Đăng nhập',
              textAlign: TextAlign.center,
              style: TextStyle
                (
                  fontSize: 20,
                  color: Colors.white,
                  fontWeight: FontWeight.bold
              ),),
            Icon(Icons.navigate_next, color: Colors.white,),
          ],
        ),
      ),
    );



    return Scaffold(
      backgroundColor: Colors.white,
      body: Center(
        child: SingleChildScrollView(
          child: Container(
            color: Colors.white,
            child: Padding(
              padding: const EdgeInsets.all(36.0),
              child: Form(
                key: _formKey,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      color: Colors.yellow,
                      child: SizedBox(
                        height: 200,
                        child: Image.network(
                          "https://file.hstatic.net/1000280264/article/logo-sendo_22379c86d6d04d809bfed4897a465d4d.png",
                          fit: BoxFit.contain,
                        ),
                      ),
                    ),
                    SizedBox(height: 45,),
                    emailField,
                    SizedBox(height: 15,),
                    passwordField,
                    SizedBox(height: 45,),
                    loginButton,
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text("Bạn chưa "),
                        GestureDetector(
                          onTap: () {
                            Navigator.push(
                                context,
                                  MaterialPageRoute(
                                builder: (context) => RegistrationScreen()));
                          },
                          child: Text("Đăng ký",
                          style: TextStyle(
                            color: Colors.redAccent,
                              fontWeight: FontWeight.bold,
                              fontSize: 15),
                          ),
                        ),
                        Text(' ?')
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
  
  void signIn(String email, String password) async
  {
    if(_formKey.currentState!.validate())
      {
        await _auth
            .signInWithEmailAndPassword(email: email, password: password)
            .then((uid) => {
              Fluttertoast.showToast(msg: "Đăng nhập thành công"),
              Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => MyMainStatefull()))
        }).catchError((e) {
          Fluttertoast.showToast(msg: e!.message);
        });
      }
  }
}
