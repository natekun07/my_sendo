import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../widget/product_sendo_widget.dart';

class PayScreen extends StatefulWidget {
  const PayScreen({Key? key}) : super(key: key);

  @override
  State<PayScreen> createState() => _PayScreenState();
}

class _PayScreenState extends State<PayScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        backgroundColor: Colors.red,
        title: Center(
          child: Text('Ví ưu đãi'),
        ),
      ),
      body: ListView(
        children: [
          product_sendo_widget(),
        ],
      ),
    );
  }
}
