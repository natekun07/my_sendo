import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:my_sendo/screen/tabbar/cloths_screen.dart';
import 'package:my_sendo/screen/tabbar/foryou_screen.dart';
import 'package:my_sendo/screen/tabbar/kitchen_utensils_screen.dart';

class SuggestScreen extends StatefulWidget {
  const SuggestScreen({Key? key}) : super(key: key);

  @override
  State<SuggestScreen> createState() => _SuggestScreenState();
}

class _SuggestScreenState extends State<SuggestScreen> {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
        length: 3,
        child: Scaffold(
          appBar: AppBar(
            automaticallyImplyLeading: false,
            elevation: 0,
            backgroundColor: Colors.red,
            title: Container(
              color: Colors.white,
              child: Column(
                children: [
                  Row(
                    children: [
                      IconButton(onPressed: (){},
                        icon: Icon(Icons.search),
                        color: Colors.grey,
                        padding: EdgeInsets.only(
                          bottom: 10.0,
                          left: 10.0,
                          right: 10.0
                      ),
                      ),
                      Expanded(child: TextField(
                        decoration: InputDecoration(
                          contentPadding: EdgeInsets.only(bottom: 10.0, left: 10.0, right: 10.0),
                          hintText: 'Tìm sản phẩm và Shop',
                          hintStyle: TextStyle(
                          color: Colors.black,
                          fontSize: 16,
                          fontStyle: FontStyle.italic,
                          ),
                        ),
                      style: TextStyle(
                      color: Colors.black,
                    ),
                  )),
                    ],
              ),
                ],
              ),
            ),
            actions: [
              Column(
                children: [
                  Expanded(
                    child: MaterialButton(
                      onPressed:(){},
                    child: Icon(Icons.paypal),
                    ),
                  ),
                  Text('Ví senPay', textAlign: TextAlign.center,style: TextStyle(fontWeight: FontWeight.bold),),
                  Text('Ưu đãi mỗi ngày', textAlign: TextAlign.center,),
            ],
          ),
              IconButton(onPressed: (){}, icon: Icon(Icons.shopping_cart)),
            ],
            bottom: createTabBar(),
          ),
          body: TabBarView(
            children: [
              ForyouScreen(),
              KitchenUtensilsCreen(),
              ClothScreen(),
            ],
          ),
        )
    );
  }
  TabBar createTabBar()  {
    return TabBar(
      labelColor: Colors.white,
      tabs: [
        Tab (child: Text("Cho bạn")),
        Tab (child: Text("Đồ dùng nhà bếp")),
        Tab (child: Text("Đầm, váy")),
      ],
      isScrollable: true,
      indicatorColor: Colors.white,
    );
  }
}
