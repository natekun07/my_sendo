import 'package:flutter/cupertino.dart';

import '../../widget/product_sendo_widget.dart';

class KitchenUtensilsCreen extends StatefulWidget {
  const KitchenUtensilsCreen({Key? key}) : super(key: key);

  @override
  State<KitchenUtensilsCreen> createState() => _KitchenUtensilsCreenState();
}

class _KitchenUtensilsCreenState extends State<KitchenUtensilsCreen> {
  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        product_sendo_widget(),
      ],
    );
  }
}
