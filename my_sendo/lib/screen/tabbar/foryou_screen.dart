import 'package:flutter/cupertino.dart';
import 'package:my_sendo/widget/product_sendo_widget.dart';

class ForyouScreen extends StatefulWidget {
  const ForyouScreen({Key? key}) : super(key: key);

  @override
  State<ForyouScreen> createState() => _ForyouScreenState();
}

class _ForyouScreenState extends State<ForyouScreen> {
  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        product_sendo_widget(),
      ],
    );
  }
}
