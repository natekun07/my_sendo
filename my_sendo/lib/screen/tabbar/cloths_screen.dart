import 'package:flutter/cupertino.dart';
import 'package:my_sendo/widget/product_sendo_widget.dart';

class ClothScreen extends StatefulWidget {
  const ClothScreen({Key? key}) : super(key: key);

  @override
  State<ClothScreen> createState() => _ClothScreenState();
}

class _ClothScreenState extends State<ClothScreen> {
  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        product_sendo_widget(),
      ],
    );
  }
}
