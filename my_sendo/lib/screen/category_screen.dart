import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:my_sendo/widget/product_sendo_widget.dart';

class CategoryScreen extends StatefulWidget {
  const CategoryScreen({Key? key}) : super(key: key);

  @override
  State<CategoryScreen> createState() => _CategoryScreenState();
}

class _CategoryScreenState extends State<CategoryScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(80),
        child: AppBar(
          automaticallyImplyLeading: false,
          backgroundColor: Colors.red,
          title: Center(
            child:  Column(
              mainAxisSize: MainAxisSize.max,
              children: [
                SizedBox(height: 15,),
                Text('Tất cả danh mục', style: TextStyle(fontSize: 16,),),
                SizedBox(height: 3,),
                Container(
                  color: Colors.white,
                  child: Column(
                    children: [
                      Row(
                        children: [
                          IconButton(
                            onPressed: (){

                          },
                            icon: Icon(Icons.search),
                            color: Colors.grey,
                            padding: EdgeInsets.only(
                                bottom: 10.0,
                                left: 10.0,
                                right: 10.0),
                          ),
                          Expanded(child: TextField(
                            scrollPadding: EdgeInsets.all(30),
                            decoration: InputDecoration(
                              contentPadding: EdgeInsets.only(bottom: 10.0, left: 10.0, right: 10.0),
                              hintText: 'Tìm sản phẩm và Shop',
                              hintStyle: TextStyle(
                                color: Colors.black,
                                fontSize: 16,
                                fontStyle: FontStyle.italic,
                              ),
                            ),
                            style: TextStyle(
                              color: Colors.black,
                            ),
                          )),
                        ],
                      ),
                    ],
                  ),
                ),

              ],
            ),
          ),
        ),
      ),
      body: ListView(
        children: [
          product_sendo_widget()
        ],
      ),
    );
  }
}
