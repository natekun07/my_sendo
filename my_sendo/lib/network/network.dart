import 'dart:convert';
import 'package:flutter/foundation.dart';
import 'package:my_sendo/models/product_sendo.dart';
import 'package:http/http.dart' as http;

List<product_sendo> parseProductCategory(String responseBody){
  var list = json.decode(responseBody) as List<dynamic>;
  List<product_sendo>? productsendo = list.map((model) => product_sendo.fromJson(model)).toList();
  return productsendo;
}

Future<List<product_sendo>> fetchProductSendo() async{
  String url = 'https://shein-products.mocklab.io/list_products_sendo';
  Uri uri = Uri.parse(url);
  final response = await http.get(uri);
  if(response.statusCode == 200)
    {
      return compute(parseProductCategory, response.body);
    } else {
    throw Exception('Request API Error');
  }
}