import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:my_sendo/network/network.dart';

class product_sendo_widget extends StatelessWidget {
  const product_sendo_widget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: fetchProductSendo(),
      builder: (
          BuildContext context,
          AsyncSnapshot snapshot
          ) {
        if(snapshot.hasData) {
          return Column(
            children: [
              GridView.builder(
                physics: ScrollPhysics(),
                itemCount: snapshot.data.length,
                itemBuilder: (BuildContext context, int index) {
                  return MaterialButton(
                    onPressed: (){
                      Navigator.push(context, MaterialPageRoute<void>(
                        builder: (BuildContext context) {
                          return Scaffold(
                            appBar: AppBar(
                              leading: BackButton(
                                  color: Colors.black
                              ),
                              titleTextStyle: TextStyle(color: Colors.black),
                              backgroundColor: Colors.white,
                              title: Text(snapshot.data[index].name),
                            ),
                          );
                        },
                      ));
                    },
                    child: Column(
                      children: [
                        Expanded(child: Image.network(snapshot.data[index].image,fit: BoxFit.fitHeight,)),
                        Expanded(child:Text(snapshot.data[index].name, textAlign: TextAlign.center,),),
                      ],
                    ),
                  );
                },
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 2,
                    mainAxisSpacing: 10,
                    crossAxisSpacing: 10
                ),
                padding: EdgeInsets.all(10),
                shrinkWrap: true,
              ),

            ],
          );
        }
        else if(snapshot.hasError){
          return Container(
            child: Center(
              child: Text("Not found!!"),
            ),
          );
        }
        else {
          return Container(
            child: Center(
              child: CircularProgressIndicator(),
            ),
          );
        }
      },
    );
  }
}